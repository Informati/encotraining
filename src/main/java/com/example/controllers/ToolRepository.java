package com.example.controllers;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ToolRepository extends CrudRepository<Tool, Integer> {
	
}
