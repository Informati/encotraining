package com.example.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class ToolsService {
	
	List<Tool> tools = new ArrayList<Tool>();
	
	public ToolsService () {
		setUpData();
	}
	
	public void setUpData() {
		tools.add(new Tool(142, "Spring Boot", "Spring boot is a Java framework to build standalone applications", 4));
		tools.add(new Tool(64, "Doker", "Doker description", 2));
		tools.add(new Tool(85, "Kubernate", "Kubernate description", 3));
		tools.add(new Tool(626, "AUTHO2", "AUTHO2 Description", 2));
	}
 	
	public List<Tool> getAllTools(){
		return tools;
	}
	
	public Tool getTool(int id) {
		return tools.stream().filter(t -> t.getId() == id).findFirst().get();
	}
	
	public void addTool(Tool tool) {
		tools.add(tool);
	}
	
	public void updateTool(Tool tool, int id) {
		for(Tool t : tools) {
			if(t.getId()==id) {
				tools.set(tools.indexOf(t), tool);
				return;
			}
		}
	}

	public void deleteTool(int id) {
		tools.removeIf(t -> t.getId() == id);
		
	}
	
	
	
	
}
