package com.example.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	 private static final Logger LOG = LoggerFactory.getLogger(HelloController.class);
	
	@Autowired
	private ToolsService toolsService;
	
	@Autowired
	private ToolRepository toolRepository;
	
	public HelloController() {
		if(toolRepository != null) {
			toolRepository.save(new Tool(142, "Spring Boot", "Spring boot is a Java framework to build standalone applications", 4));
			toolRepository.save(new Tool(64, "Doker", "Doker description", 2));
			toolRepository.save(new Tool(85, "Kubernetes", "Kubernetes description", 3));
			toolRepository.save(new Tool(626, "AUTHO2", "AUTHO2 Description", 2));
		} else 
			LOG.warn("toolRepo is not instanciated",0);
	}
	
	@RequestMapping("/")
	public String sayHi() {
		toolRepository.save(new Tool(142, "Spring Boot", "Spring boot is a Java framework to build standalone applications", 4));
		toolRepository.save(new Tool(64, "Doker", "Doker description", 2));
		toolRepository.save(new Tool(85, "Kubernate", "Kubernate description", 3));
		toolRepository.save(new Tool(626, "AUTHO2", "AUTHO2 Description", 2));
		return "hola hello there, this the home page, adding this line to be sure that the volume has been shared successfuly";
	}
	
	@RequestMapping("/tools")
	public List<Tool> getAllTools(){
		List<Tool> tools = new ArrayList<Tool>();
		for(Tool tool : toolRepository.findAll()) {
			tools.add(tool);
		}
		return tools;
	}
	
	@RequestMapping("tools/{id}")
	public Tool getTool(@PathVariable int id) {
		return toolRepository.findOne(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/tools")
	public void addTool(@RequestBody Tool tool) {
		toolRepository.save(tool);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/tools")
	public void updateTool(@RequestBody Tool tool) {
		toolRepository.save(tool);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/tools/{id}")
	public void deleteTool(@PathVariable int id) {
		toolRepository.delete(id);
	}

}
