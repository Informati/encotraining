package com.example.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Tool {
	
	@Id
	@NotNull
	private int id;
	@Size(min=2,max=16)
	private String name;
	private String description;
	private int hoursNeededToLearn;
		
	public Tool() {}
	
	
	public Tool(int id, String name, String description, int hoursNeededToLearn) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.hoursNeededToLearn = hoursNeededToLearn;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getHoursNeededToLearn() {
		return hoursNeededToLearn;
	}
	public void setHoursNeededToLearn(int hoursNeededToLearn) {
		this.hoursNeededToLearn = hoursNeededToLearn;
	}

}
