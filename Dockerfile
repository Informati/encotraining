FROM openjdk:8-jdk-alpine
COPY target/Training-0.0.1-SNAPSHOT.jar /usr/src/myapp/
WORKDIR /usr/src/myapp/
EXPOSE 8080
ENTRYPOINT ["java","-jar","Training-0.0.1-SNAPSHOT.jar"]